import { ApolloClient, gql, InMemoryCache, HttpLink } from '@apollo/client';
import fetch from 'cross-fetch';
import express from 'express';
const client = new ApolloClient({
    link: new HttpLink({
        uri: 'http://dataretrieve:3002/graphql',
        fetch
    }),
    cache: new InMemoryCache(),
    defaultOptions: {
        watchQuery: {
            fetchPolicy: 'no-cache',
            errorPolicy: 'ignore'
        },
        query: {
            fetchPolicy: 'no-cache',
            errorPolicy: 'ignore'
        }
    }
});
class Vector {
    static getLength(pointStart, pointEnd) {
        return Math.abs(Math.sqrt((pointEnd.y - pointStart.y) ** 2) + (pointEnd.x - pointStart.x) ** 2);
    }
    static getSumLength(points) {
        let sum = -1;
        let prev;
        for (let vector of points) {
            if (!prev) {
                prev = vector;
                continue;
            }
            sum += this.getLength(prev, vector);
            prev = vector;
        }
        return sum;
    }
    static getTopLeft(points) {
        let closest = { x: -1, y: -1 };
        let minDist = Number.MAX_VALUE;
        let tmpDist = 0;
        for (let vector of points) {
            tmpDist = this.getLength(this.startPoint, vector);
            if (tmpDist < minDist) {
                minDist = tmpDist;
                closest = vector;
            }
        }
        return closest;
    }
    static getBottomRight(points) {
        let closest = { x: -1, y: -1 };
        let minDist = Number.MAX_VALUE;
        let tmpDist = 0;
        for (let vector of points) {
            tmpDist = this.getLength(vector, this.endPoint);
            if (tmpDist < minDist) {
                minDist = tmpDist;
                closest = vector;
            }
        }
        return closest;
    }
}
Vector.startPoint = { x: 0, y: 0 };
Vector.endPoint = { x: 400, y: 400 };
const check = (response) => {
    return response.data.statRaw.filter((v) => v.x && v.y);
};
const getData = () => {
    return client.query({
        query: gql `
    query {
      statRaw {
        x,y
      }
    }
    `
    }).catch(err => console.error(err));
};
const app = express();
app.get('/topLeft', async (req, res) => {
    const data = check(await getData());
    const topLeftVector = Vector.getTopLeft(data);
    console.log(topLeftVector);
    res.json(topLeftVector);
});
app.get('/bottomRight', async (req, res) => {
    const data = check(await getData());
    const bottomRightVector = Vector.getBottomRight(data);
    console.log(bottomRightVector);
    res.json(bottomRightVector);
});
app.get('/sumDistance', async (req, res) => {
    const data = check(await getData());
    const totalVectorlength = Vector.getSumLength(data);
    console.log(totalVectorlength);
    res.json({ totalVectorlength });
});
app.listen(process.env.DATAANALIZE_PORT || 3103, () => {
    console.log('Listenning on port', process.env.DATAANALIZE_PORT || 3103);
});
