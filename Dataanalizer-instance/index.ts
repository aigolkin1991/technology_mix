import {
  ApolloClient,
  gql,
  InMemoryCache,
  NormalizedCacheObject,
  ApolloQueryResult,
  HttpLink
} from '@apollo/client';
import fetch from 'cross-fetch';
import express from 'express';

const client: ApolloClient<NormalizedCacheObject> = new ApolloClient({
  link: new HttpLink({
    uri: 'http://localhost:3002/graphql',
    fetch
  }),
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore'
    },
    query: {
      fetchPolicy: 'no-cache',
      errorPolicy: 'ignore'
    }
  }
})

interface vector2d {
  readonly x: number
  readonly y: number
}

class Vector {
  private static startPoint: vector2d = {x: 0, y: 0}
  private static endPoint: vector2d = {x: 400, y: 400}

  static getLength(pointStart: vector2d, pointEnd: vector2d): number {
    return Math.abs( Math.sqrt( (pointEnd.y - pointStart.y) ** 2)+(pointEnd.x - pointStart.x) ** 2 );
  }

  static getSumLength(points: vector2d[]): number {
    let sum = -1
    let prev: vector2d
    for(let vector of points) {
      if(!prev) {
        prev = vector
        continue
      }
      sum += this.getLength(prev, vector)
      prev =  vector
    }

    return sum
  }

  static getTopLeft(points: vector2d[]): vector2d {
    let closest: vector2d = {x: -1, y: -1};
    let minDist = Number.MAX_VALUE
    let tmpDist = 0
    for(let vector of points) {
      tmpDist = this.getLength(this.startPoint, vector)
      if (tmpDist < minDist) {
        minDist = tmpDist
        closest = vector
      }
    }
    return closest
  }

  static getBottomRight(points: vector2d[]): vector2d {
    let closest: vector2d = {x: -1, y: -1};
    let minDist = Number.MAX_VALUE
    let tmpDist = 0
    for(let vector of points) {
      tmpDist = this.getLength(vector, this.endPoint)
      if (tmpDist < minDist) {
        minDist = tmpDist
        closest = vector
      }
    }
    return closest
  }
}

const check = (response: ApolloQueryResult<any>): vector2d[] => {
  return response.data.statRaw.filter( (v : vector2d) => v.x && v.y )
}

const getData = () : Promise<any> => {
  return client.query({
    query: gql`
    query {
      statRaw {
        x,y
      }
    }
    `
  }).catch(err => console.error(err));
}

const app = express();

app.get('/topLeft', async (req, res) => {
  const data = check(await getData());
  const topLeftVector = Vector.getTopLeft(data)
  console.log(topLeftVector)
  res.json(topLeftVector)
})
app.get('/bottomRight', async (req, res) => {
  const data = check(await getData());
  const bottomRightVector = Vector.getBottomRight(data)
  console.log(bottomRightVector)
  res.json(bottomRightVector)
})
app.get('/sumDistance', async (req, res) => {
  const data = check(await getData());
  const totalVectorlength = Vector.getSumLength(data)
  console.log(totalVectorlength)
  res.json({ totalVectorlength })
})

app.listen(process.env.DATAANALIZE_PORT || 3103, () => {
  console.log('Listenning on port', process.env.DATAANALIZE_PORT || 3103)
})