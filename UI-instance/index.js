const express = require('express')

const app = express()

app.use('/', express.static('static'))

app.listen(process.env.UI_PORT || 3100)

