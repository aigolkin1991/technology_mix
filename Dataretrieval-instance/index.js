const express = require('express')
const { graphqlHTTP } = require('express-graphql')
const Schem = require('./schema/DataRetrieveSchema')

const app = express()
app.post('/graphql', graphqlHTTP({
  schema: Schem
}))
app.get('/graphql', graphqlHTTP({
  schema: Schem
}))

app.listen(process.env.DATARETRIEVE_PORT || 3102, () => {
  console.log('Listenning', process.env.DATARETRIEVE_PORT || 3102)
})