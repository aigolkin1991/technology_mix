const graphQLFields = require('graphql-fields')
const { PrismaClient } = require('@prisma/client')
const { GraphQLSchema, GraphQLInt, GraphQLObjectType, GraphQLID, GraphQLList } = require('graphql')

const { vector2d } = new PrismaClient()

const VetorType = new GraphQLObjectType({
  name: 'Vector',
  fields: () => ({
    id: { type: GraphQLID },
    x: { type: GraphQLInt },
    y: { type: GraphQLInt },
  })
})

const Query = new GraphQLObjectType({
  name: 'Query',
  fields: {
    statRaw: {
      type: new GraphQLList(VetorType),
      resolve: async (parent, args, ctx, info) => {
        const result = await vector2d.findMany({
          take: 2000,
          select: {x: true, y: true}
        })
        console.log(result)
        return result
      }
    }
  }
})

module.exports = new GraphQLSchema({
  query: Query
})