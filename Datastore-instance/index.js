const express = require('express');

const storeRouter = require('./router/StoreRouter')

const app = express();

app.use(express.json());
app.use(storeRouter)
app.post('/', storeRouter);
app.get('/', (req, res) => {
  res.status(404)
})

app.listen(process.env.DATASTORE_PORT || 3101, () => {
  console.info('Listenning on port', process.env.DATASTORE_PORT || 3101)
});