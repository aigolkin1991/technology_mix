const { Router } = require('express')
const { store } = require('../controller/StoreController')
const router = Router()

router.post('/store', store);

module.exports = router;