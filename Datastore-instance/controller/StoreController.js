const { PrismaClient } = require("@prisma/client");

const prismaClient = new PrismaClient()

async function store(req, res) {
  console.log('POST', req.body)
  const arrayVector = req.body;

  try {
    await prismaClient.vector2d.createMany({
      data: arrayVector
    });
  } catch(e) {
    console.error(e)
    return res.status(500).json({message:"erroor", error: e})
  }
  return res.status(200).json({message: 'ok'})
}

module.exports = {
  store
};