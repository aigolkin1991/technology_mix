-- CreateTable
CREATE TABLE "vector2d" (
    "id" SERIAL NOT NULL,
    "x" INTEGER NOT NULL DEFAULT 0,
    "y" INTEGER NOT NULL DEFAULT 0,

    PRIMARY KEY ("id")
);
